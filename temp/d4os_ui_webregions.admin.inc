<?php

/**
 * @package    d4os_ui_webregions
 * @copyright Copyright (C) 2010-2017 Wene - ssm2017 Binder ( S.Massiaux ). All rights reserved.
 * @link      http://www.d4os.org
 * @license   GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * D4os is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/**
 * Admin panel
 */
function d4os_ui_webregions_settings() {
  $form = array();
  $form['password']['d4os_ui_webregions_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('d4os_ui_webregions_password', '1234'),
    '#description' => t('Password to be able to read simulator settings'),
    '#required' => TRUE
  );
  return system_settings_form($form);
}
