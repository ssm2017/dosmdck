<?php

/**
 * @package    d4os_ui_webregions
 * @copyright Copyright (C) 2010-2017 Wene - ssm2017 Binder ( S.Massiaux ). All rights reserved.
 * @link      http://www.d4os.org
 * @license   GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * D4os is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/**
 * Admin panel
 */
function dosmdck_settings() {
  $form = array();
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => 'API',
    '#description' => t('Settings to be able to reach the Phposdck API.'),
  );
  $form['api']['dosmdck_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Phposdck API url'),
    '#default_value' => variable_get('dosmdck_api_url', 'http://localhost'),
    '#description' => t('Url of the Phposdck API'),
    '#required' => TRUE
  );
  $form['api']['dosmdck_api_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Phposdck API port'),
    '#default_value' => variable_get('dosmdck_api_port', '4550'),
    '#description' => t('Port of the Phposdck API'),
    '#required' => TRUE
  );
  $form['dosmdck_radmin_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Default radmin pass'),
    '#default_value' => variable_get('dosmdck_radmin_pass', '1234'),
    '#description' => t('Default radmin password'),
    '#required' => TRUE
  );
  return system_settings_form($form);
}
